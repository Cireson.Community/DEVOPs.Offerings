﻿/* 
v3.8.2
Tested for v3.8
Contributors: Nick Velich

The "Assign to Analyst By Group" task allows you to assign a Work item to a Support Group and an Analyst within that Support Group.

But what happens if you select a Support Group but do not select an analyst? Currently, the Portal allows you to omit selection of an analyst, 
and the following scenario can occur:

---
Support Group 1: Contains Analyst 1
Support Group 2: Contains Analyst 2

Work Item originally assigned to Support Group 1 and Analyst 1. The "Assign to Analyst By Group" task is used to assign to Support Group 2, but the "Assigned To"
field is omitted. The Work Item is then assigned to Support Group 2 with Analyst 1 still as the Assigned To. We do not want this to occur.
---

The following code will clear the Assigned To field whenever (1) the "Assign To Analyst By Group" task is used and an Analyst is not selected; and
(2) The Support Group field on the form directly is cleared.
*/
app.custom.formTasks.add('ServiceRequest', null, function (formObj, viewModel) {
	formObj.boundChange("SupportGroup",function (formObj, viewModel) {
		console.log("Support Group has changed");
		var vm = pageForm.viewModel;

		var newSupportGroupId = vm.SupportGroup.Id;
		 
		if(newSupportGroupId == ""){
			if(vm.AssignedWorkItem != null && vm.AssignedWorkItem.DisplayName != null){
				console.log("Clearing assigned to");
				vm.AssignedWorkItem.BaseId = null;
				vm.AssignedWorkItem.DisplayName = null;
				vm.AssignedWorkItem.set("AssignedWorkItem", { BaseId: '', DisplayName: '' });          
			}
		}
    });
});

app.custom.formTasks.add('Incident', null, function (formObj, viewModel) {
	formObj.boundChange("SupportGroup",function (formObj, viewModel) {
		 console.log("Support Group has changed");
		 var vm = pageForm.viewModel;
		 
		 var newSupportGroupId = vm.SupportGroup.Id;
		   
		if(newSupportGroupId == ""){
			if(vm.AssignedWorkItem != null && vm.AssignedWorkItem.DisplayName != null){
				console.log("Clearing assigned to");
				vm.AssignedWorkItem.BaseId = null;
				vm.AssignedWorkItem.DisplayName = null;
				vm.AssignedWorkItem.set("AssignedWorkItem", { BaseId: '', DisplayName: '' });  
			}
		}
    });
});