$(document).ready(function () {
	if (session.user.AssetManager == 1 || session.user.IsAdmin === true)
	{
		var softwareAssetNav = $(".nav_trigger").find("h4:contains(Software Assets)").first().parent(); //THere "should" always only be one. But do first() just in case. 
		if (softwareAssetNav.length === 0) {
			setTimeout(
				function() {
					//re-obtain the hardware asset nav node. 
					fn_AddSoftwareAssetSearchField();
				}, 
			1500);
		}
		else
			fn_AddSoftwareAssetSearchField(softwareAssetNav);
		
		function fn_AddSoftwareAssetSearchField (navNodeDiv) {
			if (navNodeDiv === undefined) 
				navNodeDiv = $(".nav_trigger").find("h4:contains(Software Assets)").first().parent(); 
			$(navNodeDiv).append("<div class='customassetsearch' style='margin-left: 10px; margin-top: 10px;'>" + 
									"<div style='color:#fff; font-family:'400 18px/22px',?'Segoe UI',?'Open Sans',?Helvetica,?sans-serif; display:inline-block; width: 100%;'>" + 
										"Software Asset Search:&nbsp;" + 
									"</div>" + 
									"<input type='text' id='searchSoftwareAsset' style='color: #000000; width: 95%;' />" + 
								"</div>");
			$("#searchSoftwareAsset").kendoAutoComplete({
				dataTextField: "DisplayName",
				filter: "contains",
				placeholder: "Type the name...",
				minLength: 3,
				delay: 500,
				dataSource: { 
					type: "json",
					serverFiltering: true,
					transport: {
						read: {
							url:"/api/V3/Config/GetConfigItemsByAbstractClass?userId=" + session.user.Id + "&isUserScoped=false&objectClassId=81e3da4f-e41c-311e-5b05-3ca779d030db" 
						},
						parameterMap: function (options) {
								return { searchFilter: options.filter.filters[0].value };
						}
					}
				},
				select: function(e) {
					var newWindow = window.open('/AssetManagement/SoftwareAsset/Edit/' + this.dataItem(e.item.index()).Id, '_blank');
					newWindow.focus();
				},
				change: function(e) {
					$("#searchSoftwareAsset").data("kendoAutoComplete").value(""); 
				}
			});
		}
	}
});