$(document).ready(function () {
	if (session.user.AssetManager == 1 || session.user.IsAdmin === true)
	{
		var hardwareAssetNav = $(".nav_trigger").find("h4:contains(Hardware Assets)").first().parent(); //THere "should" always only be one. But do first() just in case. 
		if (hardwareAssetNav.length === 0) {
			setTimeout(
				function() {
					//re-obtain the hardware asset nav node. 
					fn_AddHardwareAssetSearchField();
				}, 
			1500);
		}
		else
			fn_AddHardwareAssetSearchField(hardwareAssetNav);
		
		function fn_AddHardwareAssetSearchField (navNodeDiv) {
			if (navNodeDiv === undefined) 
				navNodeDiv = $(".nav_trigger").find("h4:contains(Hardware Assets)").first().parent(); //THere "should" always only be one. But do first() just in case. 
			$(navNodeDiv).append("<div class='customassetsearch' style='margin-left: 10px; margin-top: 10px;'>" + 
									"<div style='color:#fff; font-family:'400 18px/22px',?'Segoe UI',?'Open Sans',?Helvetica,?sans-serif; display:inline-block; width: 100%;'>" + 
										"Hardware Asset Search:&nbsp;" + 
									"</div>" + 
									"<input type='text' id='searchAsset' style='color: #000000; width: 95%;' />" + 
								"</div>");
			$("#searchAsset").kendoAutoComplete({
				dataTextField: "DisplayName",
				filter: "contains",
				placeholder: "Type the name...",
				minLength: 3,
				delay: 500,
				dataSource: { 
					type: "json",
					serverFiltering: true,
					transport: {
						read: {
							url:"/api/V3/Config/GetConfigItemsByAbstractClass?userId=" + session.user.Id + "&isUserScoped=false&objectClassId=c0c58e7f-7865-55cc-4600-753305b9be64" 
						},
						parameterMap: function (options) {
								return { searchFilter: options.filter.filters[0].value };
						}
					}
				},
				select: function(e) {
					var newWindow = window.open('/AssetManagement/HardwareAsset/Edit/' + this.dataItem(e.item.index()).Id, '_blank');
					newWindow.focus();
				},
				change: function(e) {
					$("#searchAsset").data("kendoAutoComplete").value(""); 
				}
			});
		}
		
	}
});