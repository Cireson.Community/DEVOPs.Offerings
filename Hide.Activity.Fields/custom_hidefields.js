﻿//start hide activity fields
app.custom.formTasks.add('ServiceRequest', null, function(formObj, viewModel){  
      formObj.boundReady(function () {
            var targetFields = [
                  "Area",
                  "Priority",
                  "Stage",
                  "Scheduled Start",
                  "Scheduled End",
                  "Impacted Configuration Items"
            ]
            
            fn_HideActivityFields(targetFields);
      });
});
      
function fn_HideActivityFields (fields) {
    $("div.panel-heading:contains('Activities')").parent().on("mouseenter", function (){
        console.log("I'm hiding fields!", fields);
        for(var i=0; i < fields.length; i++){
                var label = fields[i];
                var item = $("div.activity-item-form").find("label span:contains(" + label + ")");
                $(item).each(function () {
                    var ctrl = $(this).parents("div")[2];
                    $(ctrl).hide();
                });
        }  
    });
}
//END hide activity fields