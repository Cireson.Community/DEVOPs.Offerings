# Hide Activity Fields

Provides the code to hide fields on activities. This is dynamic and you can add additional field names as desired.

*Currently only works for Service Requests but can be easily adapted to support Change Requests*