# Dynamic Work Item Forms

This script works with both service requests, but can be easily adapted to work with any work item class in the Cireson portal.

### Service Request Project Forms

With this example, we use a TemplateId field in the Service Request class, and have it set to _PROJECT_.

When then show a customized tab for the project and hide the general tab. This way you can have a specific layout per work item based on the template id.

### Installation

Simply copy the contents of this file to your customspace\custom.js file.

You will also need to make sure and set the template ID within your service request template that you are using as the target template/request type. You can do this by exporting the templates management pack and adding this property to the xml of the template:

<Property Path="$Context/Property[Type='CustomSystem_WorkItem_ServiceRequest_Library!System.Workitem.ServiceRequest']/TemplateId$">ProjectRequest</Property>