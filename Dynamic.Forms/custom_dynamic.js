/* Service Request Tasks */
app.custom.formTasks.add('ServiceRequest', null, function(formObj, viewModel){  	
	formObj.boundReady( function () {
		if(viewModel.TemplateId == 'ProjectRequest') {
			fn_ShowTab("PROJECT", formObj)
		}else{
			fn_HideTab("PROJECT", formObj)
		}
	});	
}); 

function fn_ShowTab(name,form){
	console.log("This is a Non-Standard Request");
	$('a[data-toggle=tab]').each(function(){
		if(this.innerText === name){
			$(this).trigger('click');
		} else if (this.innerText === "GENERAL") {
			$(this).hide();
		}
	});
}

function fn_HideTab(name,form){
	console.log("This is NOT a Non-Standard Request");
	$('a[data-toggle=tab]').each(function(){
		if(this.innerText === name){
			$(this).hide();
		}
	});
}