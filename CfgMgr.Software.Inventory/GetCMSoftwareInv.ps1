﻿
##################################################
#
# NAME: CMSoftwareInv.ps1 
#
# DESCRIPTION: 
# This script dumps softwae inventory data from 
# SC Configuration Manager 2012 R2.
#
# INSTRUCTIONS FOR USE:
# 1. Run on Site Server with CM console installed
# 2. Collect output CMSoftware.csv from the 
#    C:\Windows\Temp directory.
#
##################################################

#Import ConfigMgr PowerShell Module 
Import-Module (Join-Path $(Split-Path $env:SMS_ADMIN_UI_PATH) ConfigurationManager.psd1) 

# CD to the ConfigMgr drive 
$CMProvider = (Get-PSDrive -PSProvider CMSite).Name

$CMDrive = $CMProvider + ':'

CD $CMDrive

# Dump software inventory
Get-CMSoftwareInventory | Select-Object CategoryName, CommonName, CommonPublisher, CommonVersion, FamilyID | export-csv -path c:\windows\temp\CMSoftware.csv -NoTypeInfo 

