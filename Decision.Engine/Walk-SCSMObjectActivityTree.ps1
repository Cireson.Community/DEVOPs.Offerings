﻿param($objName)

function WalkActivityTree {
    param($baseWI,$spacer="")

    $activities = GetAllActivities($baseWI)

    foreach($activity in $activities) {
        if($activity.ClassName -match "ParallelActivity") {
            write-host -foregroundcolor white ($spacer +  "---------------------------")
            write-host -foregroundcolor white ($spacer +  "Name                 : " + $activity.Name)
            write-host -foregroundcolor white ($spacer +  "Title                : " + $activity.Title)
            write-host -foregroundcolor yellow ($spacer + "Status               : " + $activity.Status.DisplayName)
            write-host -foregroundcolor gray ($spacer +   "ID                   : " + $activity.get_id())
            write-host -foregroundcolor white ($spacer +  "Stage                : " + $activity.Stage.DisplayName)
            write-host -foregroundcolor white ($spacer +  "Seq ID               : " + $activity.SequenceId)
            write-host -foregroundcolor white ($spacer +  "Decision Template ID : " + $activity.DecisionTemplateId)
            WalkActivityTree $activity ($spacer+"`t")
        } elseif($activity.ClassName -match "SequentialActivity") {
            write-host -foregroundcolor white ($spacer +  "---------------------------")
            write-host -foregroundcolor white ($spacer +  "Name                 : " + $activity.Name)
            write-host -foregroundcolor white ($spacer +  "Title                : " + $activity.Title)
            write-host -foregroundcolor yellow ($spacer + "Status               : " + $activity.Status.DisplayName)
            write-host -foregroundcolor gray ($spacer +   "ID                   : " + $activity.get_id())
            write-host -foregroundcolor white ($spacer +  "Stage                : " + $activity.Stage.DisplayName)
            write-host -foregroundcolor white ($spacer +  "Seq ID               : " + $activity.SequenceId)
            write-host -foregroundcolor white ($spacer +  "Decision Template ID : " + $activity.DecisionTemplateId)
            WalkActivityTree $activity ($spacer+"`t")
        } elseif ($activity.ClassName -match "ManualActivity") {
            write-host -foregroundcolor white ($spacer +  "---------------------------")
            write-host -foregroundcolor white ($spacer +  "Name                 : " + $activity.Name)
            write-host -foregroundcolor white ($spacer +  "Title                : " + $activity.Title)
            write-host -foregroundcolor yellow ($spacer + "Status               : " + $activity.Status.DisplayName)
            write-host -foregroundcolor gray ($spacer +   "ID                   : " + $activity.get_id())
            write-host -foregroundcolor white ($spacer +  "Stage                : " + $activity.Stage.DisplayName)
            write-host -foregroundcolor white ($spacer +  "Seq ID               : " + $activity.SequenceId)
            write-host -foregroundcolor white ($spacer +  "Decision Template ID : " + $activity.DecisionTemplateId)
            #This IF is specific to MPAA and finding and checking the decision extension of an MP
            if($activity.ActivityDecision) {
                write-host -foregroundcolor Green ($spacer + "Decision             : " + $activity.ActivityDecision.DisplayName)
            }
        } else {
            write-host -foregroundcolor white ($spacer +  "---------------------------")
            write-host -foregroundcolor white ($spacer +  "Name                 : " + $activity.Name)
            write-host -foregroundcolor white ($spacer +  "Title                : " + $activity.Title)
            write-host -foregroundcolor yellow ($spacer + "Status               : " + $activity.Status.DisplayName)
            write-host -foregroundcolor gray ($spacer +   "ID                   : " + $activity.get_id())
            write-host -foregroundcolor white ($spacer +  "Stage                : " + $activity.Stage.DisplayName)
            write-host -foregroundcolor white ($spacer +  "Seq ID               : " + $activity.SequenceId)
            write-host -foregroundcolor white ($spacer +  "Decision Template ID : " + $activity.DecisionTemplateId)
        }
    }
}

function GetAllActivities {
    param($baseWI)

    $relContainsActGUID = "2da498be-0485-b2b2-d520-6ebd1698e61b"
    $relContainsActClass = Get-SCSMRelationshipClass -Id $relContainsActGUID

    foreach($obj in $baseWI) {
        $act += Get-SCSMRelatedObject -Relationship $relContainsActClass -SMObject $obj
    }
    return $act | sort sequenceid
}

#INITIALIZE

if (!(Get-Module smlets)) {Import-Module smlets -force -ErrorAction stop}

#START SCRIPT
if($objName -match "SR")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.ServiceRequest$) -Filter "Name -eq $objName"

}
elseif($objName -match "CR")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.ChangeRequest$) -Filter "Name -eq $objName"

}
elseif($objName -match "MA")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Activity.ManualActivity$) -Filter "Name -eq $objName"

}
elseif($objName -match "RA")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Activity.ReviewActivity$) -Filter "Name -eq $objName"

}
elseif($objName -match "SA")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Activity.SequentialActivity$) -Filter "Name -eq $objName"

}
elseif($objName -match "PA")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Activity.ParallelActivity$) -Filter "Name -eq $objName"
}
elseif($objName -match "IR")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Incident$) -Filter "Name -eq $objName"
}
else 
{
    $type = $objName.substring(0,2)
    Write-Host "Unsupported type: $type"
}

write-host -foregroundcolor white ($spacer +  "Name                 : " + $obj.Name)
write-host -foregroundcolor white ($spacer +  "Title                : " + $obj.Title)
write-host -foregroundcolor yellow ($spacer + "Status               : " + $obj.Status.DisplayName)
write-host -foregroundcolor gray ($spacer +   "ID                   : " + $obj.get_id())
write-host -foregroundcolor white ($spacer +  "Area                 : " + $obj.Area.DisplayName)
write-host -foregroundcolor white ($spacer +  "Decision Template ID : " + $obj.DecisionTemplateId)

WalkActivityTree $obj "`t"