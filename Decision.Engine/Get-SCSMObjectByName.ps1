﻿param($objName)

if($objName -match "SR")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.ServiceRequest$) -Filter "Name -eq $objName"
    return $obj
}
elseif($objName -match "CR")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.ChangeRequest$) -Filter "Name -eq $objName"
    return $obj
}
elseif($objName -match "MA")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Activity.ManualActivity$) -Filter "Name -eq $objName"
    return $obj
}
elseif($objName -match "RA")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Activity.ReviewActivity$) -Filter "Name -eq $objName"
    return $obj
}
elseif($objName -match "SA")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Activity.SequentialActivity$) -Filter "Name -eq $objName"
    return $obj
}
elseif($objName -match "PA")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Activity.ParallelActivity$) -Filter "Name -eq $objName"
    return $obj
}
elseif($objName -match "IR")
{
    $obj = Get-SCSMObject -Class (Get-SCSMClass -Name System.WorkItem.Incident$) -Filter "Name -eq $objName"
    return $obj
}
else 
{
    $type = $objName.substring(0,2)
    Write-Host "Unsupported type: $type"
}