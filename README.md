# Project Description

This project maintains all of the public code for Cireson's DEVOPs offerings. We will place code here as examples of Cireson's consulting team has to offer as well as examples for public consumption to benefit the Microsoft System Center community as a whole.

### Disclaimer

All files and projects located here come as is and without any warranty or support. We will attempt to improve the projects as time goes on based on customer and community demand. Comments and improvements are welcome as well as customization requests.

Cireson's support team has no information on these projects outside of what you have available and will not provide support for these enhancements, extensions, and scripts.


### Contact Us

We can be contacted via our sales team from the link below. You can also follow us on Twitter as @teamcireson
<http://www.cireson.com>