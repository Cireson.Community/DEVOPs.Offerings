﻿//Add Manual Activity
app.custom.formTasks.add('ServiceRequest', null, function (formObj, viewModel) {
	formObj.boundReady(function(){
	var vm = pageForm.viewModel;
	//If the Service Request is Completed don't show the Add MA task
		if (vm.Status.Name == "Completed"){
			$("li[class='link']:contains('Add MA')").css("display", "none");
		}
	});
	return;
});

//Add Manual Activity
app.custom.formTasks.add('ServiceRequest', "Add MA", function (formObj, viewModel) {	
	var vm = pageForm.viewModel;
	var activities = [];
	var length = vm.Activity.length;
	//Find the Activity that is In Progress
	var startpos = 0;
	for (i=0; i < length; ++i) {
		if (vm.Activity[i].Status.Name == "In Progress") {
			startpos = i;
		}
	}
	console.log (startpos + " - " + vm.Activity[startpos].Title)
	//Create Activity Dropdown values that start with the Activity In Progress
	for (i=startpos; i < length; ++i) {
			activities.push({
			"SequenceId": vm.Activity[i].SequenceId,
			"Title": vm.Activity[i].Id + " - " + vm.Activity[i].Title,
			"Type": vm.Activity[i].ClassName
		});
	}
	//console.log(activities);
	enterMAtitle(activities);

});
//Add Manual Activity
function enterMAtitle (activities){
	//use requirejs to load the HTML template first
	require(["text!/CustomSpace/customtasks.addma.html"], function (template) {
		//make a jQuery obj
		cont = $(template);

		//create a view model to handle the UX
		//if there are no activities currently then disable the drop-down menu
		if (activities.length == 0) {
		var _vmWindow = new kendo.observable({
			paselected: false,
			dropenabled: false,
			okClick: function () {
				var sequenceid = $("#activityselected option:selected").val();
				var title = this.get("title");
				var desc = this.get("description");
				//They clicked OK now call the Add Manual Activity function and send Title
				addMAtoSR(pageForm.viewModel, title, desc, sequenceid);
				customWindow.close();				
			},
			cancelClick: function () {
				customWindow.close();
			}
		});	
		}
		//if there are activities currently then enable the drop-down menu and pass current activities
		else {
		var _vmWindow = new kendo.observable({
			paselected: false,
			dropenabled: true,
			dropDownData: activities,
			valueChanged : function(e) {
				var dataItem = e.sender.dataItem();
				//console.log (dataItem.Type)
				if (dataItem.Type == "System.WorkItem.Activity.ParallelActivity"){
					this.set("paselected", true)
				}
				else {
					this.set("paselected", false)
				}
			},
			okClick: function () {
				var sequenceid = $("#activityselected option:selected").val();
				var title = this.get("title");
				var desc = this.get("description");
				var inpa = this.get("withinPA");
				//They clicked OK now call the Add Manual Activity function and send Title
				addMAtoSR(pageForm.viewModel, title, desc, sequenceid, inpa);
				//console.log(inpa)
				customWindow.close();				
			},
			cancelClick: function () {
				customWindow.close();
			}
		});	 
		}
		//create the kendo window
		customWindow = cont.kendoWindow({
			title: "Add Manual Activity",
			resizable: false,
			modal: true,
			viewable: false,
			width: 500,
			height: 400,
			close: function () {
	 
			},
			activate: function () {
				//on window activate bind the view model to the loaded template content
				kendo.bind(cont, _vmWindow);
			}
		}).data("kendoWindow");
	 
		//now open the window
		customWindow.open().center();
	});
}
//Add Manual Activity
function addMAtoSR (servicerequest, title, desc, sequenceid, inpa){
	//console.log("Sequence Id: " + sequenceid);
	//console.log(desc);
	var ogsr = servicerequest;
	var finalsr = JSON.parse(JSON.stringify(ogsr));
	var srid = ogsr.Id;
	var length = ogsr.Activity.length;
	//if there are no activities currently just add the Activity
	if (length == 0){
		finalsr.Activity.push({
						"ClassTypeId": "7ac62bd4-8fce-a150-3b40-16a39a61383d",
						"SequenceId": 0,
						"Title": title,
						"Description": desc,
						"Id": "MA{0}",
					});
	}
	//They are adding the Activity within a Parallel Activity
	else if (inpa == true) {
		var palength = ogsr.Activity[sequenceid].Activity.length;
		//console.log (palength);
		var newseqid = palength + 1;
		//console.log (newseqid);
		//If the Parallel Activity is In Progress make the Manual Activity In Progress
		if (ogsr.Activity[sequenceid].Status.Name == "In Progress") {
					finalsr.Activity[sequenceid].Activity.push({
						"ClassTypeId": "7ac62bd4-8fce-a150-3b40-16a39a61383d",
						"SequenceId": newseqid,
						"Title": title,
						"Description": desc,
						"Id": "MA{0}",
						"Status": {"Id": "11fc3cef-15e5-bca4-dee0-9c1155ec8d83"}
					});
		}
		//If the Parallel Activity is not In Progress make the Manual Activity Pending
		else {
					finalsr.Activity[sequenceid].Activity.push({
						"ClassTypeId": "7ac62bd4-8fce-a150-3b40-16a39a61383d",
						"SequenceId": newseqid,
						"Title": title,
						"Description": desc,
						"Id": "MA{0}"
					});
		}		
	}
	//They are adding an Activity and it isn't within a Parallel Activity
	else {
		var newpos = parseInt(sequenceid) + 1;
						finalsr.Activity.push({
						"ClassTypeId": "7ac62bd4-8fce-a150-3b40-16a39a61383d",
						"SequenceId": newpos,
						"Title": title,
						"Description": desc,
						"Id": "MA{0}",
					});

			for (var i = newpos; i < length; i++) {
				finalsr.Activity[i].SequenceId = i + 1;
			}
	}
	var strData = { "formJson":{"original": ogsr, "current": finalsr}}
	//console.log (strData);
		$.ajax({
			url: "/api/V3/Projection/Commit",
			type: "POST",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify(strData) ,
			success: function (data2) {
				window.location.href = window.location.protocol + "//" + window.location.hostname + "/ServiceRequest/Edit/" + srid
			}
		});

}