{
    "Default":{
    tabList: [
        /*********/
        /** TAB **/
        /*********/
        {
            name: "General",
            content: [
                {
                    customFieldGroupList: [
                        {
                            name: "IncidentInformation",
                            rows: [                                
                                {
                                    columnFieldList: [
                                        { DataType: "UserPicker", PropertyDisplayName: "AffectedUser", PropertyName: "RequestedWorkItem" },
                                        { DataType: "String", PropertyDisplayName: "Alternatecontactmethod", PropertyName: "ContactMethod", ColSpan: 2, MinLength: 0, MaxLength: 256 }
                                    ],
                                },
                                {
                                    columnFieldList: [
                                        { DataType: "String", PropertyDisplayName: "Title", PropertyName: "Title", Required: true, MinLength: 0, MaxLength: 200 }
                                    ],
                                },
                                {
                                    columnFieldList: [
                                        { DataType: "LongString", PropertyDisplayName: "Description", PropertyName: "Description", MinLength: 0, MaxLength: 4000 }
                                    ],
                                },
                                { 
                                    columnFieldList:
                                    [
                                        { DataType: "Enum", PropertyDisplayName: "Classification", PropertyName: "Classification", EnumId: '1f77f0ce-9e43-340f-1fd5-b11cc36c9cba' },
                                        { DataType: "Enum", PropertyDisplayName: "Source", PropertyName: "Source", EnumId: '5d59071e-69b3-7ef4-6dee-aacc5b36d898' },
                                    ]
                                },
                                {
                                    columnFieldList:
                                    [
                                            { DataType: "Enum", PropertyDisplayName: "Impact", PropertyName: "Impact", EnumId: '11756265-f18e-e090-eed2-3aa923a4c872', Required: true },
                                            { DataType: "Enum", PropertyDisplayName: "Urgency", PropertyName: "Urgency", EnumId: '04b28bfb-8898-9af3-009b-979e58837852', Required: true },
                                            { DataType: "String", PropertyDisplayName: "Priority", PropertyName: "Priority", Disabled: true },
                                    ]
                                },
                                {
                                    columnFieldList: 
                                    [
                                        { DataType: "Enum", PropertyDisplayName: "SupportGroup", PropertyName: "TierQueue", EnumId: 'c3264527-a501-029f-6872-31300080b3bf'},
                                        { DataType: "UserPicker", PropertyDisplayName: "AssignedTo", PropertyName: "AssignedWorkItem", FilterByAnalyst: true  },
                                        { DataType: "UserPicker", PropertyDisplayName: "PrimaryOwner", PropertyName: "RelatesToIncident", FilterByAnalyst: true},
                                    ]
                                },
                                {
                                    columnFieldList: [
                                        { DataType: "Boolean", PropertyDisplayName: "Escalated", PropertyName: "Escalated", Required: false, Inline: true }
                                    ],
                                }
                            ]
                        },                        
                        {
                            name: "ActionLog",
                            type: "actionLog"
                        },
                        {
                            name: "AffectedConfigurationItems",
                            type: "affectedItems"
                        }
                    ]
                }]
        },
        /*********/
        /** TAB **/
        /*********/
		/*
        {
            name: "Activities",
            content: [
                {
                    customFieldGroupList: [
                        {
                            name: "Activities",
                            type: "activities"
                        }
                    ]
                }
            ]
        },
		*/
        /*********/
        /** TAB **/
        /*********/
        {
            name: "RelatedItems",
            content: [
                {
                    customFieldGroupList: [
                        {
                            name: "RelatedConfigurationItems",
                            type: "relatedItems"
                        },
                        {
                            name: "ChildWorkIems",
                            type: "childWorkItems"
                        },
                        {
                            name: "FileAttachments",
                            type: "fileAttachments"
                        },
                        {
                            name: "WorkItems",
                            type: "multipleObjectPicker",
                            PropertyName: "RelatesToWorkItem",
                            ClassId: "f59821e2-0364-ed2c-19e3-752efbb1ece9",
                            PropertyToDisplay: {Id:"Id",Title:"Title",Status:"Status", LastModified:"LastModified"},
                            SelectableRow: true,
                            SelectProperty: "Id"
                        }

                    ]
                }
            ]
        },
        /*********/
        /** TAB **/
        /*********/
        {
            name: "Resolution",
            content: [
                {
                    customFieldGroupList: [
                        {
                            name: "Resolution",
                            rows:[
                                {
                                    columnFieldList: [
                                        { DataType: "DateTime", PropertyDisplayName: "Resolveddate", PropertyName: "ResolvedDate", Disabled: true },
                                        { DataType: "UserPicker", PropertyDisplayName: "ResolvedByUser", PropertyName: "RelatesToTroubleTicket", Disabled: true }
                                    ],
                                }
                            ]
                        },
                        {
                            name: "TimeWorked",
                            type: "billableTime"
                        },
                    ]
                }
            ]
        },
		/*********/
        /** TAB **/
        /*********/
        {
            name: "Extensions",
            content: [
                {
					columnFieldList: [
						{ DataType: "String", PropertyDisplayName: "IR_String_01", PropertyName: "IR_String_01" },
						{ DataType: "String", PropertyDisplayName: "IR_String_02", PropertyName: "IR_String_02" },
						{ DataType: "String", PropertyDisplayName: "IR_String_03", PropertyName: "IR_String_03" },
						{ DataType: "String", PropertyDisplayName: "IR_String_04", PropertyName: "IR_String_04" },
						{ DataType: "String", PropertyDisplayName: "IR_String_05", PropertyName: "IR_String_05" },
						{ DataType: "String", PropertyDisplayName: "IR_String_06", PropertyName: "IR_String_06" },
						{ DataType: "String", PropertyDisplayName: "IR_String_07", PropertyName: "IR_String_07" },
						{ DataType: "String", PropertyDisplayName: "IR_String_08", PropertyName: "IR_String_08" },
						{ DataType: "String", PropertyDisplayName: "IR_String_09", PropertyName: "IR_String_09" },
						{ DataType: "String", PropertyDisplayName: "IR_String_10", PropertyName: "IR_String_10" },
						{ DataType: "String", PropertyDisplayName: "IR_String_11", PropertyName: "IR_String_11" },
						{ DataType: "String", PropertyDisplayName: "IR_String_12", PropertyName: "IR_String_12" },
						{ DataType: "String", PropertyDisplayName: "IR_String_13", PropertyName: "IR_String_13" },
						{ DataType: "String", PropertyDisplayName: "IR_String_14", PropertyName: "IR_String_14" },
						{ DataType: "String", PropertyDisplayName: "IR_String_15", PropertyName: "IR_String_15" },
						{ DataType: "String", PropertyDisplayName: "IR_String_16", PropertyName: "IR_String_16" },
						{ DataType: "DateTime", PropertyDisplayName: "IR_Date_01", PropertyName: "IR_Date_01" },
						{ DataType: "DateTime", PropertyDisplayName: "IR_Date_02", PropertyName: "IR_Date_02" },
						{ DataType: "DateTime", PropertyDisplayName: "IR_Date_03", PropertyName: "IR_Date_03" },
						{ DataType: "DateTime", PropertyDisplayName: "IR_Date_04", PropertyName: "IR_Date_04" },
						{ DataType: "Boolean", PropertyDisplayName: "IR_Bool_01", PropertyName: "IR_Bool_01" },
						{ DataType: "Boolean", PropertyDisplayName: "IR_Bool_02", PropertyName: "IR_Bool_02" },
						{ DataType: "Boolean", PropertyDisplayName: "IR_Bool_03", PropertyName: "IR_Bool_03" },
						{ DataType: "Boolean", PropertyDisplayName: "IR_Bool_04", PropertyName: "IR_Bool_04" },
					],
				},
            ]
        },
        /*********/
        /** TAB **/
        /*********/
        {
            name: "History",
            content: [
                {
                    customFieldGroupList: [
                        {
                            name: "History",
                            type: "history"
                        }
                    ]
                }
            ]
        }
    ]
	},
    "DefaultEndUser":{
        tabList: [
            /*********/
            /** TAB **/
            /*********/
            {
                name: "General",
                content: [
                    {
                        customFieldGroupList: [
                            {
                                name: "IncidentInformation",
                                rows: [                                
                                    {
                                        columnFieldList: [
                                            { DataType: "UserPicker", PropertyDisplayName: "AffectedUser", PropertyName: "RequestedWorkItem", Disabled:true },
                                            { DataType: "String", PropertyDisplayName: "Alternatecontactmethod", PropertyName: "ContactMethod", ColSpan: 2, MinLength: 0, MaxLength: 256 }
                                        ],
                                    },
                                    {
                                        columnFieldList: [
                                            { DataType: "String", PropertyDisplayName: "Title", PropertyName: "Title", Required: true, MinLength: 0, MaxLength: 200, Disabled:true }
                                        ],
                                    },
                                    {
                                        columnFieldList: [
                                            { DataType: "LongString", PropertyDisplayName: "Description", PropertyName: "Description", MinLength: 0, MaxLength: 4000, Disabled:true }
                                        ],
                                    }
                                ]
                            },                        
                            {
                                name: "ActionLog",
                                type: "actionLog"
                            },
                            {
                                name: "FileAttachments",
                                type: "fileAttachments"
                            }
                        ]
                    }]
            }
        ]
    },
}