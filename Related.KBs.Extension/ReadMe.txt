Description:
	This custom extension will perform a weighted search against the KB search portal API and then perform a relevance analysis against the IR/SR/CR and return 
	the top 5 most relevent results and create both a notification at the top for the most relevent and a list on the right for the top 5 for easy access to relevent KBs


Requirements:
	v3.6+ of the Cireson Portal

Installation:
	Add the following lines to your custom.js file located in the CustomSpace folder of the Cireson Portal website directory:
		
		/* Incident Tasks */
		app.custom.formTasks.add('Incident', null, function(formObj, viewModel){  	
			formObj.boundReady( function () {

				getRelatedKBs();

			});	
		});

		/* Service Request Tasks */
		app.custom.formTasks.add('ServiceRequest', null, function(formObj, viewModel){  	
			formObj.boundReady( function () {

				getRelatedKBs();
		
			});
		}); 

		/* Change Request Tasks */
		app.custom.formTasks.add('ChangeRequest', null, function(formObj, viewModel){  		
			formObj.boundReady( function () {

				getRelatedKBs();

			});
		});

	Below this code in the custom.js file, you will need to paste the contents of the ext_RelatedKBs.js
	Save your custom.js and then refresh your browser

Configuration:
	None required.