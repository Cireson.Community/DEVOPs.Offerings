Description:
	This custom extension will perform a search against incidents assigned to the logged in analyst and display 5 items in each incident form that have applied SLAs to them.
	It will choose the first 5 results showing only breached and warning state SLA


Requirements:
	v3.6+ of the Cireson Portal

Installation:
	Add the following lines to your custom.js file located in the CustomSpace folder of the Cireson Portal website directory:
		
		/* Incident Tasks */
		app.custom.formTasks.add('Incident', null, function(formObj, viewModel){  	
			formObj.boundReady( function () {

				slaReminders();

			});	
		});

	Below this code in the custom.js file, you will need to paste the contents of the ext_SLAReminder.js
	Save your custom.js and then refresh your browser

Configuration:
	None required.