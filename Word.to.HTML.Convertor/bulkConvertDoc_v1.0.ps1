﻿#bulk doc to html converter for the cireson portal
#will convert doc to html with base 64 images for the KB
#this requires LibreOffice installed : https://www.libreoffice.org/download/libreoffice-fresh

$libreOfficePath = 'C:\Program Files (x86)\LibreOffice 4\program\soffice.exe'
$htmlDir = 'C:\Users\Will\Documents\CiresonPortal\HTML'
$docDir = 'C:\Users\Will\Documents\CiresonPortal\DOC'


function convert-to-html {
    param ($docFiles,$destinationPath)

    foreach ($doc in $docFiles) {
        $docPath = $doc.FullName
        &($libreOfficePath) --convert-to html:"HTML" $docPath --outdir $destinationPath --headless

        #the conversion takes time, let's wait until we find the converted file before continuing
        do{

            $fileExists = $false
            $fileExists = Test-Path ($destinationPath + '/' + $doc.BaseName + '.html')
            Start-Sleep -Seconds 2

        }while ($fileExists -eq $false)
    }
}

##MAIN

#get all docs

$allDocsToConvert = get-childitem $docDir

#make sure libreoffice is not running
$libreOfficeProcess = $null
$libreOfficeProcess = get-process | ?{$_.path -eq $libreOfficePath}
if( $libreOfficeProcess-ne $null ){
    #exe is running. let's kill it with fire!
    Stop-Process $libreOfficeProcess
}

#convert all documents

convert-to-html -docFiles $allDocsToConvert -destinationPath $htmlDir