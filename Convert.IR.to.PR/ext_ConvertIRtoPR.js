/*START Convert IR to PR extension */
function convertIRtoPR () {
    //Class Id for Problem
    var classid = "422afc88-5eff-f4c5-f8f6-e01038cde67f";
    //Call the Get Templates Web Service to return all Problem Templates
    $.ajax({
        url: "/api/V3/Template/GetTemplates",
        data: {classId: classid},
        type: "GET",
        success: function (data) {
            console.log (data)
            //Call the loadTemplates Function and send it the PR Template Names and Id's
            loadTemplates(data);
        }
    });

    function loadTemplates (templateData){
        //use requirejs to load the HTML template first
        require(["text!/CustomSpace/ext_ConvertIRtoPR_template.html"], 
            function (htmlTemplate) {
                //make a jQuery obj
                templateObj = $(htmlTemplate);

                //create a view model to handle the UX
                var _vmWindow = new kendo.observable({
                    dropDownData: templateData,
                    valueChanged : function(e) {
                        var dataItem = e.sender.dataItem();
                        console.log (dataItem.Id)
                    },
                    okClick: function () {
                        var templateId = $("#templateselected option:selected").val();
                        console.log(templateId);
                        //They clicked OK now call the Create Problem function and send the Template ID that is selected
                        createProblem(templateId, pageForm.viewModel);
                        customWindow.close();
                    },
                    cancelClick: function () {
                        customWindow.close();

                    }
                });
             
                //create the kendo window
                customWindow = templateObj.kendoWindow({
                    title: "Convert to Problem",
                    resizable: false,
                    modal: true,
                    viewable: false,
                    width: 500,
                    height: 300,
                    close: function () {
             
                    },
                    activate: function () {
                        //on window activate bind the view model to the loaded template content
                        kendo.bind(templateObj, _vmWindow);
                    }
                }).data("kendoWindow");
             
                //now open the window
                customWindow.open().center();
            }
        );
    }

    function createProblem (templateId, vm_Incident) {
        //Problem ProjectionID 'System.WorkItem.Problem.ProjectionType'
        var projId = "45c1c404-f3fe-1050-dcef-530e1c2533e1"; //NOT USED CURRENTLY
        //Logged in User Id
        var uid = session.user.Id;
        //console.log(uid);
        $.ajax({
            url: "/api/V3/Projection/CreateProjectionByTemplate",
            data: {id: templateId, createdById: uid},
            type: "GET",
            success: function (data) {
                console.log(data);

                //Get the Incident information and copy it to the new Problem
                data.Title = vm_Incident.Title;
                data.Description = vm_Incident.Description;
                data.ContactMethod = vm_Incident.ContactMethod
                data.RequestedWorkItem = [{
                    ClassTypeId: vm_Incident.RequestedWorkItem.ClassTypeId,
                    BaseId: vm_Incident.RequestedWorkItem.BaseId,
                    DisplayName: vm_Incident.RequestedWorkItem.DisplayName
                }];
                data.CreatedWorkItem = [{
                    ClassTypeId: "eca3c52a-f273-5cdc-f165-3eb95a2b26cf",
                    BaseId: session.user.Id,
                    DisplayName: session.user.Name
                }];
                data.RelatesToWorkItem = [{
                    ClassTypeId: vm_Incident.ClassTypeId,
                    BaseId: vm_Incident.BaseId,
                    Id: vm_Incident.Id
                }];
                /* Work through all WI related to IR and attach them to PR */
                for (var i=0; i < vm_Incident.RelatesToWorkItem.length; i++) { 
                    console.log(vm_Incident.RelatesToWorkItem[i].BaseId)
                    
                    data.RelatesToWorkItem.push({
                        ClassTypeId: vm_Incident.RelatesToWorkItem[i].ClassTypeId,
                        BaseId: vm_Incident.RelatesToWorkItem[i].BaseId,
                        Id: vm_Incident.RelatesToWorkItem[i].Id
                    });
                };
                data.NameRelationship = [{
                    Name: "RequestedWorkItem",
                    RelationshipId: "DFF9BE66-38B0-B6D6-6144-A412A3EBD4CE"
                },
                {
                    Name: "RelatesToWorkItem",
                    RelationshipId: "cb6ce813-ea8d-094d-ee5a-b755701f4547"
                },
                {
                    Name: "CreatedWorkItem",
                    RelationshipId: "df738111-c7a2-b450-5872-c5f3b927481a"
                },
                {
                    Name: "RelatesToConfigItem",
                    RelationshipId: "d96c8b59-8554-6e77-0aa7-f51448868b43"
                }];
                /* Work through all CI related to IR and attach them to PR */
                data.RelatesToConfigItem = [];
                for (var i=0; i < vm_Incident.RelatesToConfigItem.length; i++) { 
                    console.log(vm_Incident.RelatesToConfigItem[i].BaseId)
                    
                    data.RelatesToConfigItem.push({
                        ClassTypeId: vm_Incident.RelatesToConfigItem[i].ClassTypeId,
                        BaseId: vm_Incident.RelatesToConfigItem[i].BaseId,
                        Name: vm_Incident.RelatesToConfigItem[i].Name
                    });
                };
                data.Impact = {
                    Id: "80cc222b-2653-2f68-8cee-3a7dd3b723c1",
                    Name: "Medium",
                    uid: "31334f2f-f4ff-4bca-84cb-9c624583af79"
                };   
                data.Urgency = {
                    Id: "02625c30-08c6-4181-b2ed-222fa473280e",
                    Name: "Medium",
                    uid: "1581258d-b196-46f4-9ac0-4abf73d3a66c"  
                };                                                            
                //console.log (data)
                var prId = data.Id;
                var strData = { "formJson":{"current": data }}
                $.ajax({
                    url: "/api/V3/Projection/Commit",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(strData) ,
                    success: function (data2) {
                        setIRresolved(vm_Incident);
                        window.location.href = window.location.protocol + "//" + window.location.hostname + "/Problem/Edit/" + prId
                    }
                });                                 
            }
        });
    }

    function setIRresolved (incident){
        var newIR = JSON.stringify(incident);
        var finalIR = JSON.parse(newIR);
        finalIR.Status.Id="2b8830b6-59f0-f574-9c2a-f4b4682f1681";
        finalIR.ResolutionDescription = "This Incident was converted to a Problem."
        finalIR.ResolutionCategory.Id = "c5f6ada9-a0df-01d6-7087-6b8500ca6c2b";   
        finalIR.ResolvedDate = new Date().toISOString().split(".")[0];
        
        var uid = session.user.Id;
        var displayName = session.user.Name;
        console.log(new Date().toISOString().split(".")[0]);
        finalIR.AppliesToWorkItem = [{
            "ActionType": {
              "Id": "5ca2cfee-6740-1576-540B-ce17222840b8",
              "Name": "Record Resolved"
            },  
            "Description": "This Incident was converted to a Problem.",
            "EnteredBy": displayName,
            "EnteredDate": new Date().toISOString().split(".")[0],
            "LastModified": new Date().toISOString().split(".")[0],
            "Title": "Record Resolved",
            "Image": "/Content/Images/Icons/ActionLogIcons/recordresolved.png",
            "BillableTime": {
              "BasedId": null,
              "DisplayName": null
            },
            "LastUpdatedDisplay": null      
        }];
        finalIR.RelatesToTroubleTicket = [{
            "ClassTypeId": "eca3c52a-f273-5cdc-f165-3eb95a2b26cf",
            "BaseId": uid,
            "DisplayName": displayName
        }];
        var strData = { "formJson":{"original": incident, "current": finalIR}}
        console.log (strData);
            $.ajax({
                url: "/api/V3/Projection/Commit",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(strData) ,
                success: function (d) {
                    console.log(d);
                }
            });
    }
}