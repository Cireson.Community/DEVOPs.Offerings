﻿#variables from SCORCH
$sr_guid = "8b40199e-1c60-c60b-d0f9-5cbd9da2db7b"


#import external modules
Import-Module smlets

#scsm variables
$oob_requested_by_rel_obj = Get-SCSMRelationshipClass -name 'System.WorkItemRequestedByUser$'
$cireson_requested_by_rel_obj = Get-SCSMRelationshipClass -name 'RequestedByForCiresonPortal$'
$oobRequestedByUser = $null
$ciresonRequestedByUser = $null

#get the new SR
$sr_obj = get-scsmobject -id $sr_guid

#get the user object that is at the other end of the 'requested by' relationship
$oobRequestedByUser = Get-SCSMRelatedObject -SMObject $sr_obj -Relationship $oob_requested_by_rel_obj
$ciresonRequestedByUser = Get-SCSMRelatedObject -SMObject $sr_obj -Relationship $cireson_requested_by_rel_obj

#$oobRequestedByUser

#set the user object if not null
if (($oobRequestedByUser -ne $null) -AND ($ciresonRequestedByUser -eq $null)) {
    New-SCSMRelationshipObject -Relationship $cireson_requested_by_rel_obj -Source $sr_obj -Target $oobRequestedByUser -Bulk
    #Set-scsmobject -SMObject $sr_obj -Property Notes -Value 'Updated by SCORCH'
}

#to test
#$typeProjection_obj = Get-SCSMTypeProjection -name 'Cireson.ServiceRequest.ViewModelRequestedBy'
#$proj_obj = Get-SCSMObjectProjection -Projection $typeProjection_obj -Filter "id -eq $($sr_obj.id)"

#test user
#$user_obj = get-scsmobject -class (get-scsmclass -name 'Microsoft.AD.User$') | select -first 1

#unload modules
Remove-Module smlets