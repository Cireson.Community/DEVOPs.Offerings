Synopsis:
	Microsoft includes a 'Requested By' relationship out of the box in SCSM. Many customers end up using this, only to find that it
	isn't possible to expose this field in the Cireson Portal, since it has the same 'target' as the 'Created By' user. This
	collection of files gives customers another field to use as their 'Requested By' for tracking purposes.

Files:
	Cireson - Remap Requested By User.ois_export - Orchestrator Workflow to Remap from OOB to new Cireson Extension
		- Use if you have existing console forms targeting the OOB relationship
		- Modify it to re-map existing work items
	Cireson.ServiceRequest.NewRequestedByRel.mp - Ready to import, sealed MP
		- Sealed with the Cireson Dev Ops Sealing Key (included)
		- Contains the additional relationship, and an updated Type Projection for the Cireson Portal
	Cireson.ServiceRequest.NewRequestedByRel.xml - Unsealed, source MP
		- You may seal this with your own sealing key
		- Contains the additional relationship, and an updated Type Projection for the Cireson Portal
	CiresonPortalFormCode.txt - Line for the ServiceRequest.js Cireson Portal Form Modification
		- You will also have to create a custom form target in the file, and add that, along with the projection ID to the 
		admin settings of your cireson portal
	Remap OOB RequestedBy.ps1 - Powershell code from the Orchestrator Workflow

