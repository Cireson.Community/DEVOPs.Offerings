﻿#PARAMATERS =============================================================================================
param ($SCSMServer = "SCSM01",$startingAsset = $null)

[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

if (!(Get-Module -Name SMLets)) { Import-Module SMLets -ErrorAction Stop }

#Base Form ==============================================================================================
$mainForm = New-Object System.Windows.Forms.Form
$mainForm.Size = New-Object System.Drawing.Size(810, 500) #Add 40 to ending form object
$mainForm.StartPosition = 'CenterScreen'
$mainForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedToolWindow
$mainForm.Text = "Search Hardware Assets"

#Form Elements ==========================================================================================
$custodianLabel = New-Object System.Windows.Forms.Label
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 10
$System_Drawing_Point.Y = 10
$custodianLabel.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 12
$custodianLabel.Size = $System_Drawing_Size #Ends at 30
$custodianLabel.Text = "Custodian Name"
$mainForm.Controls.Add($custodianLabel)

$custodianSearch = New-Object System.Windows.Forms.TextBox
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 10
$System_Drawing_Point.Y = 25
$custodianSearch.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 20
$custodianSearch.Size = $System_Drawing_Size #Ends at 30
$custodianSearch.Text = ""
$custodianSearch.add_KeyDown({ if ($_.KeyCode -eq 'Enter') { SubmitSearch(1) } })
$mainForm.Controls.Add($custodianSearch)

$userNameLabel = New-Object System.Windows.Forms.Label
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 170
$System_Drawing_Point.Y = 10
$userNameLabel.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 12
$userNameLabel.Size = $System_Drawing_Size #Ends at 30
$userNameLabel.Text = "Enterprise ID"
$mainForm.Controls.Add($userNameLabel)

$userNameSearch = New-Object System.Windows.Forms.TextBox
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 170
$System_Drawing_Point.Y = 25
$userNameSearch.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 20
$userNameSearch.Size = $System_Drawing_Size #Ends at 30
$userNameSearch.Text = ""
$userNameSearch.add_KeyDown({ if ($_.KeyCode -eq 'Enter') { SubmitSearch(2) } })
$mainForm.Controls.Add($userNameSearch)

$assetTagLabel = New-Object System.Windows.Forms.Label
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 330
$System_Drawing_Point.Y = 10
$assetTagLabel.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 12
$assetTagLabel.Size = $System_Drawing_Size #Ends at 30
$assetTagLabel.Text = "Asset Tag"
$mainForm.Controls.Add($assetTagLabel)

$assetTagSearch = New-Object System.Windows.Forms.TextBox
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 330
$System_Drawing_Point.Y = 25
$assetTagSearch.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 20
$assetTagSearch.Size = $System_Drawing_Size #Ends at 30
$assetTagSearch.Text = ""
$assetTagSearch.add_KeyDown({ if ($_.KeyCode -eq 'Enter') { SubmitSearch(3) } })
$mainForm.Controls.Add($assetTagSearch)

$serialNumberLabel = New-Object System.Windows.Forms.Label
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 490
$System_Drawing_Point.Y = 10
$serialNumberLabel.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 12
$serialNumberLabel.Size = $System_Drawing_Size #Ends at 30
$serialNumberLabel.Text = "Serial Number"
$mainForm.Controls.Add($serialNumberLabel)

$serialNumberSearch = New-Object System.Windows.Forms.TextBox
$System_Drawing_Point = New-Object System.Drawing.Point
$System_Drawing_Point.X = 490
$System_Drawing_Point.Y = 25
$serialNumberSearch.Location = $System_Drawing_Point
$System_Drawing_Size = New-Object System.Drawing.Size
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 20
$serialNumberSearch.Size = $System_Drawing_Size #Ends at 30
$serialNumberSearch.Text = ""
$serialNumberSearch.add_KeyDown({ if ($_.KeyCode -eq 'Enter') { SubmitSearch(4) } })
$mainForm.Controls.Add($serialNumberSearch)

$searchStatusLabel = New-Object System.Windows.Forms.Label
$System_Drawing_Point.X = 650
$System_Drawing_Point.Y = 25
$searchStatusLabel.Location = $System_Drawing_Point
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 20
$searchStatusLabel.Size = $System_Drawing_Size #Ends at 140
$searchStatusLabel.Text = "Press Enter to Search"
$mainForm.Controls.Add($searchStatusLabel)

$searchGrid = New-Object System.Windows.Forms.DataGridView
$System_Drawing_Point.X = 10
$System_Drawing_Point.Y = 50
$searchGrid.Location = $System_Drawing_Point
$System_Drawing_Size.Width = ($mainForm.Size.Width - 20)
$System_Drawing_Size.Height = 360
$searchGrid.Size = $System_Drawing_Size #Ends at 140
$searchGrid.TabIndex = 0
$searchGrid.ReadOnly = $true
$searchGrid.SelectionMode = 'FullRowSelect'
$searchGrid.MultiSelect = $true
$searchGrid.AutoSizeColumnsMode = 'AllCells'
$mainForm.Controls.Add($searchGrid)

$buttonDisposeAsset = New-Object System.Windows.Forms.Button
$System_Drawing_Point.X = 10
$System_Drawing_Point.Y = 420
$buttonDisposeAsset.Location = $System_Drawing_Point
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 40
$buttonDisposeAsset.Size = $System_Drawing_Size
$buttonDisposeAsset.Text = "Dispose Asset"
$buttonDisposeAsset.add_Click({ DisposeHardwareAsset })
$mainForm.Controls.Add($buttonDisposeAsset)

$buttonUnassociateCustodian = New-Object System.Windows.Forms.Button
$System_Drawing_Point.X = 170
$System_Drawing_Point.Y = 420
$buttonUnassociateCustodian.Location = $System_Drawing_Point
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 40
$buttonUnassociateCustodian.Size = $System_Drawing_Size
$buttonUnassociateCustodian.Text = "Unassociate Custodian"
$buttonUnassociateCustodian.add_Click({ UnassociateCustodian })
$mainForm.Controls.Add($buttonUnassociateCustodian)

$buttonAssociateLoanedAsset = New-Object System.Windows.Forms.Button
$System_Drawing_Point.X = 330
$System_Drawing_Point.Y = 420
$buttonAssociateLoanedAsset.Location = $System_Drawing_Point
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 40
$buttonAssociateLoanedAsset.Size = $System_Drawing_Size
$buttonAssociateLoanedAsset.Text = "Associate Loaned Asset"
$buttonAssociateLoanedAsset.add_Click({ AssociateLoanedAsset })
$mainForm.Controls.Add($buttonAssociateLoanedAsset)

$buttonAssociateCustodian = New-Object System.Windows.Forms.Button
$System_Drawing_Point.X = 490
$System_Drawing_Point.Y = 420
$buttonAssociateCustodian.Location = $System_Drawing_Point
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 40
$buttonAssociateCustodian.Size = $System_Drawing_Size
$buttonAssociateCustodian.Text = "Associate Custodian"
$buttonAssociateCustodian.add_Click({ AssociateCustodian })
$mainForm.Controls.Add($buttonAssociateCustodian)

$buttonChangeStatus = New-Object System.Windows.Forms.Button
$System_Drawing_Point.X = 650
$System_Drawing_Point.Y = 420
$buttonChangeStatus.Location = $System_Drawing_Point
$System_Drawing_Size.Width = 150
$System_Drawing_Size.Height = 40
$buttonChangeStatus.Size = $System_Drawing_Size
$buttonChangeStatus.Text = "Change Status"
$buttonChangeStatus.add_Click({ ChangeStatus })
$mainForm.Controls.Add($buttonChangeStatus)

#GLOBAL VARIABLES ======================================================================================================
$criteria = $null

#PRELOAD LOCATIONS
$jobIdLocations = (Start-Job -ScriptBlock {
	param ($SCSMServer)
	
	if (!(Get-Module -Name SMLets)) { Import-Module SMLets -ErrorAction Stop }
	
	$locClass = Get-SCSMClass -Name Cireson.AssetManagement.Location$ -ComputerName $SCSMServer
	$locations = Get-SCSMObject -Class $locClass -ComputerName $SCSMServer
	
	return $locations
} -Args $SCSMServer).Id
$locationsList = $null #global variable for pulling the status list results later

#PRELOAD STATUS LIST
$jobIdStatus = (Start-Job -ScriptBlock {
	param ($SCSMServer)
	
	if (!(Get-Module -Name SMLets)) { Import-Module SMLets -ErrorAction Stop }
	
	$enum = Get-SCSMEnumeration -Name Cireson.AssetManagement.HardwareAssetStatusEnum$ -ComputerName $SCSMServer
	$enumChildren = Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer
	
	return $enumChildren
} -Args $SCSMServer).Id
$statusList = $null #global variable for pulling the status list results later

#FUNCTIONS =============================================================================================================
function DisableButtons
{
	$buttonAssociateCustodian.Enabled = $false
	$buttonAssociateLoanedAsset.Enabled = $false
	$buttonDisposeAsset.Enabled = $false
	$buttonUnassociateCustodian.Enabled = $false
	$buttonChangeStatus.Enabled = $false
}

function EnableButtons
{
	$buttonAssociateCustodian.Enabled = $true
	$buttonAssociateLoanedAsset.Enabled = $true
	$buttonDisposeAsset.Enabled = $true
	$buttonUnassociateCustodian.Enabled = $true
	$buttonChangeStatus.Enabled = $true
}

function BuildCriteria($property,$operator,$value)
{
	$Script:criteria = @'
<Criteria xmlns="http://Microsoft.EnterpriseManagement.Core.Criteria/">
  <Reference Id="CiresonAssetManagement" PublicKeyToken="98ba2176e2a9efbc" Version="7.5.60.0" Alias="CiresonAssetManagement" />
  <Reference Id="System.Library" PublicKeyToken="31bf3856ad364e35" Version="7.5.8501.0" Alias="System" />
  <Expression>
    <SimpleExpression>
      <ValueExpressionLeft>
        <Property>{1}</Property>
      </ValueExpressionLeft>
      <Operator>{2}</Operator>
      <ValueExpressionRight>
        <Value>{3}</Value>
      </ValueExpressionRight>
    </SimpleExpression>
  </Expression>
</Criteria>
'@
	#replace property value
	$Script:criteria = $criteria.Replace('{1}', $property)
	#replace operator value
	$Script:criteria = $criteria.Replace('{2}', $operator)
	#replace search value
	$Script:criteria = $criteria.Replace('{3}', $value)
}

function LoadGrid
{
	#Set search status label
	$searchStatusLabel.Text = "Searching.... Please Wait"
	$searchStatusLabel.Refresh()
	
	#SETUP VARIABLES
	$projection = Get-SCSMTypeProjection -ComputerName $SCSMServer | ?{ $_.Name -eq "Cireson.AssetManagement.HardwareAsset.ProjectionType" }
	$cType = "Microsoft.EnterpriseManagement.Common.ObjectProjectionCriteria"
	$cObj = new-object $cType $criteria, $projection.__Base, $projection.managementgroup
	
	#SEARCH FOR ASSET AND FORMAT THE RETURNED RESULTS TO ONLY INCLUDE REQUIRED INFORMATION
	$hwAssetsObj = get-scsmobjectprojection -criteria $cObj -ComputerName $SCSMServer -MaxCount 250 | select `
		@{ Name = "Asset Name"; Expression = { $_.DisplayName } },`
		@{ Name = "Asset Tag"; Expression = { $_.AssetTag } },`
		@{ Name = "Serial #"; Expression = { $_.SerialNumber } },`
		@{ Name = "Status"; Expression = { $_.HardwareAssetStatus.displayName } },`
		@{ Name = "Custodian"; Expression = { $_.OwnedBy.displayName } }, `
		@{ Name = "Enterprise ID"; Expression = { $_.OwnedBy.userName } },`
		@{ Name = "Primary User"; Expression = { $_.Target_HardwareAssetHasPrimaryUser.displayName } },`
		@{ Name = "Location"; Expression = { $_.Target_HardwareAssetHasLocation } },`
		@{ Name = "Business OU"; Expression = { $_.Target_HardwareAssetHasCostCenter } },`
		@{ Name = "Warranty Expire"; Expression = { $_.Target_HardwareAssetHasWarranty.WarrantyEndDate } }, `
        @{ Name = "Disposal Date"; Expression = { $_.DisposalDate } }
	
	#FILL THE SEARCH GRID
	$array = New-Object System.Collections.ArrayList
	if ($hwAssetsObj -ne $null)
	{
		$array.AddRange([array]$hwAssetsObj)
		$searchGrid.DataSource = $array
	}
	else { $searchGrid.DataSource = $null }
	$searchGrid.Refresh()
	
	#DISABLE BUTTONS IF GRID EMPTY & ENABLE THEM ON FULL GRID
	if ($searchGrid.Rows.Count -eq 0)
	{
		DisableButtons
	}
	else
	{
		EnableButtons
	}
	
	#Set search status label
	$searchStatusLabel.Text = "Complete: " + $searchGrid.RowCount.ToString() + " rows"
	$searchStatusLabel.Refresh()
}

function SearchByAssetName($asset)
{
	$property = @'
$Context/Property[Type='CiresonAssetManagement!Cireson.AssetManagement.HardwareAsset']/Name$
'@
	$operator = 'Like'
	$value = '%' + $asset+ '%'
	
	BuildCriteria $property $operator $value
	
	LoadGrid
}

function SearchByCustodian
{
	$property = @'
$Context/Path[Relationship='System!System.ConfigItemOwnedByUser' TypeConstraint='System!System.Domain.User']/Property[Type='System!System.Domain.User']/UserName$
'@
	$operator = 'Like'
	$value = '%' + $userNameSearch.Text.ToString() + '%'
	
	BuildCriteria $property $operator $value
	
	LoadGrid
}

function SearchByDisplayName
{
	$property = @'
$Context/Path[Relationship='System!System.ConfigItemOwnedByUser' TypeConstraint='System!System.Domain.User']/Property[Type='System!System.Domain.User']/DisplayName$
'@
	$operator = 'Like'
	$value = '%' + $custodianSearch.Text.ToString() + '%'
	
	BuildCriteria $property $operator $value
	
	LoadGrid
}

function SearchByAssetTag
{
	$property = @'
$Context/Property[Type='CiresonAssetManagement!Cireson.AssetManagement.HardwareAsset']/AssetTag$
'@
	$operator = 'Like'
	$value = '%' + $assetTagSearch.Text.ToString() + '%'
	
	BuildCriteria $property $operator $value
	
	LoadGrid
}

function SearchBySerialNumber
{
	$property = @'
$Context/Property[Type='CiresonAssetManagement!Cireson.AssetManagement.HardwareAsset']/SerialNumber$
'@
	$operator = 'Like'
	$value = '%' + $serialNumberSearch.Text.ToString() + '%'
	
	BuildCriteria $property $operator $value
	
	LoadGrid
}

function SubmitSearch($selection)
{
	if ($selection -eq 1) { SearchByDisplayName; ClearSearchBoxes($selection) }
	elseif ($selection -eq 2) { SearchByCustodian; ClearSearchBoxes($selection) }
	elseif ($selection -eq 3) { SearchByAssetTag; ClearSearchBoxes($selection) }
	elseif ($selection -eq 4) { SearchBySerialNumber; ClearSearchBoxes($selection) }
	else
	{
		ClearSearchBoxes($selection)
		$searchGrid.DataSource = $null
		$searchGrid.Refresh()
	}
}

function ClearSearchBoxes($selection)
{
	if ($selection -eq 1) { $userNameSearch.Text = ""; $assetTagSearch.Text = ""; $serialNumberSearch.Text = "" }
	elseif ($selection -eq 2) { $custodianSearch.Text = ""; $assetTagSearch.Text = ""; $serialNumberSearch.Text = "" }
	elseif ($selection -eq 3) { $custodianSearch.Text = ""; $userNameSearch.Text = ""; $serialNumberSearch.Text = "" }
	elseif ($selection -eq 4) { $custodianSearch.Text = ""; $userNameSearch.Text = ""; $assetTagSearch.Text = "" }
	else { $custodianSearch.Text = ""; $userNameSearch.Text = ""; $assetTagSearch.Text = ""; $serialNumberSearch.Text = "" }
}

function DisposeHardwareAsset
{
	function SubmitChanges
	{
		#GET ASSET CLASS
		$hwaClass = Get-SCSMClass -Name Cireson.AssetManagement.HardwareAsset$ -ComputerName $SCSMServer
		#GET ENUMERATION VALUES
		$statusEnum = Get-SCSMEnumeration -Name Cireson.AssetManagement.HardwareAssetStatusEnum.Disposed$ -ComputerName $SCSMServer
		
		foreach ($row in $searchGrid.SelectedRows)
		{
			$assetName = $row.Cells["Asset Name"].Value.ToString()
			$hwa = Get-SCSMObject -Class $hwaClass -Filter "DisplayName -eq $assetName" -ComputerName $SCSMServer
			
			#REMOVE ANY EXISTING CUSTODIAN
			Get-SCSMRelationshipObject -BySource $hwa -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
			
			#SET OTHER REQUIRED VALUES
			$propertyHash = @{
				"DisposalDate" = $datepickerDisposed.Value;
				"LoanedDate" = $null;
				"LoanReturnedDate" = $null;
				"HardwareAssetStatus" = $statusEnum
			}
			
			$hwa | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
		}
		LoadGrid
		$subForm.Close()
	}
	
	$subForm = New-Object System.Windows.Forms.Form
	$subForm.Size = New-Object System.Drawing.Size(225, 130) #Add 40 to ending form object
	$subForm.StartPosition = 'CenterScreen'
	$subForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedToolWindow
	$subForm.Text = "Dispose Hardware Asset"
	
	#Form Elements
	$labelDisposalDate = New-Object System.Windows.Forms.Label
	$labelDisposalDate.Location = New-Object System.Drawing.Size(10, 10)
	$labelDisposalDate.Size = New-Object System.Drawing.Size(200, 20) #Ends at 30
	$labelDisposalDate.Text = "Select the Disposed Date"
	$subForm.Controls.Add($labelDisposalDate)
	
	$datepickerDisposed = New-Object System.Windows.Forms.DateTimePicker
	$datepickerDisposed.Location = New-Object System.Drawing.Size(10, 30)
	$datepickerDisposed.Size = New-Object System.Drawing.Size(200, 20) #Ends at 50
	$subForm.Controls.Add($datepickerDisposed)
	
	$buttonSubmit = New-Object System.Windows.Forms.Button
	$buttonSubmit.Location = New-Object System.Drawing.Size(10, 60)
	$buttonSubmit.Size = New-Object System.Drawing.Size(200, 30) #Ends at 140
	$buttonSubmit.Text = "Submit"
	$buttonSubmit.add_Click({ SubmitChanges })
	$subForm.Controls.Add($buttonSubmit)
	
	#display form
	$subForm.Add_Shown({ $subForm.Activate() })
	[void] $subForm.ShowDialog()
}

function UnassociateCustodian
{
	#Functions	
	function SubmitChanges
	{
		$buttonSubmit.Text = "Please wait..." #Updates button text for status
		$buttonSubmit.Refresh()
		
		#get location object and relationship
		$locClass = Get-SCSMClass -Name Cireson.AssetManagement.Location$ -ComputerName $SCSMServer
		$loc = Get-SCSMObject -Class $locClass -Filter "DisplayName -eq $($locationSelector.SelectedItem.ToString())" -ComputerName $SCSMServer
		$locRelationship = Get-SCSMRelationshipClass -Id '98dad5a4-3880-7a7e-c2ae-b5a89aa964ae' -ComputerName $SCSMServer
		
		$hwaClass = Get-SCSMClass -Name Cireson.AssetManagement.HardwareAsset$ -ComputerName $SCSMServer
		foreach ($row in $searchGrid.SelectedRows)
		{
			#SET ASSET VARIABLE FOR CURRENT ASSET NAME
			$assetName = $row.Cells["Asset Name"].Value.ToString()
			$hwa = Get-SCSMObject -Class $hwaClass -Filter "DisplayName -eq $assetName" -ComputerName $SCSMServer
			
			#Retreive the user class object and all users that match the search string
			$enum = Get-SCSMEnumeration -Name Cireson.AssetManagement.HardwareAssetStatusEnum$ -ComputerName $SCSMServer
			
			# Gets the date to be used for the assigned date.
			$date = Get-Date -Format G
			
			#Remove existing location (if one exists) and add selected location
			Get-SCSMRelationshipObject -BySource $hwa -Filter "RelationshipId -eq '98dad5a4-3880-7a7e-c2ae-b5a89aa964ae'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
			New-SCSMRelationshipObject -Relationship $locRelationship -Source $hwa -Target $loc -Bulk -ComputerName $SCSMServer
			
			#Remove related custodian
			Get-SCSMRelationshipObject -BySource $hwa -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
			
			#Blank out required values
			$propertyHash = @{
				"AssignedDate" = $date;
				"LoanedDate" = $null;
				"LoanReturnedDate" = $null;
				"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq "In Stock" })
			}
			$hwa | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
		}
		
		#Close the form on submit and refresh data in grid
		$subForm.Close()
		LoadGrid
	}
	$subForm = New-Object System.Windows.Forms.Form
	$subForm.Size = New-Object System.Drawing.Size(300, 140)
	$subForm.StartPosition = 'CenterScreen'
	$subForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedToolWindow
	$subForm.Text = "Unassociate Custodian"
	
	#Form Elements
	$labelLocations = New-Object System.Windows.Forms.Label
	$labelLocations.Location = New-Object System.Drawing.Size(10, 10)
	$labelLocations.Size = New-Object System.Drawing.Size(250, 20)
	$labelLocations.Text = "Select New Location:"
	$subForm.Controls.Add($labelLocations)
	
	$locationSelector = New-Object System.Windows.Forms.ComboBox
	$locationSelector.Location = New-Object System.Drawing.Size(10, 30)
	$locationSelector.Size = New-Object System.Drawing.Size(280, 20)
	$locationSelector.DropDownHeight = 75
	$subForm.Controls.Add($locationSelector)
	
	$buttonSubmit = New-Object System.Windows.Forms.Button
	$buttonSubmit.Location = New-Object System.Drawing.Size(100, 60)
	$buttonSubmit.Size = New-Object System.Drawing.Size(100, 30)
	$buttonSubmit.Text = "Submit"
	$buttonSubmit.add_Click({ SubmitChanges })
	$subForm.Controls.Add($buttonSubmit)
	
	#LOADS THE LOCATIONS LIST AFTER BUTTON PRESS
	if (Get-Job -Id $jobIdLocations -ErrorAction 'SilentlyContinue')
	{
		while ((Get-Job -Id $jobIdLocations).State -eq 'Running') { Start-Sleep -Milliseconds 5 }
		$Script:locationsList = Receive-Job -Id $jobIdLocations | sort DisplayName
		foreach ($location in $locationsList)
		{
			$locationSelector.Items.Add($location)
		}
		Remove-Job -Id $jobIdLocations
	}
	else
	{
		foreach ($location in $locationsList)
		{
			$locationSelector.Items.Add($location)
		}
	}
	
	#Display Form (SHOULD ALWAYS BE LAST LINE)
	$subForm.Add_Shown({ $subForm.Activate() })
	[void] $subForm.ShowDialog()
}

function AssociateLoanedAsset
{
	#Functions
	function searchForUser
	{
		$buttonSearch.Text = "Please wait..." #Updates button text for status
		$buttonSearch.Refresh()
		
		#Reset text box back to defaults and remove existing items
		$listUsers.BackColor = 'White'
		$listUsers.Items.Clear()
		
		#Retreive the user class object and all users that match the search string
		$class = Get-SCSMClass -Name Microsoft.AD.User$ -ComputerName $SCSMServer
		$users = Get-SCSMObject -Class $class -Filter "Username -like $('*' + $textUsername.Text.ToString() + '*')" -ComputerName $SCSMServer
		
		#Fill the list box with the users that match the query
		foreach ($user in $users)
		{
			$listUsers.Items.Add($user.Username)
		}
		
		$buttonSearch.Text = "Search" #Updates button text for status
		$buttonSearch.Refresh()
	}
	
	function SubmitChanges
	{
		#STOP THE SUBMIT AND HIGHLIGHT THE REQUIRED VALUE
		if ($listUsers.SelectedItem -eq $null)
		{
			$listUsers.BackColor = 'LightPink'
			return 1
		}
		
		#UPDATE BUTTON LABEL TO LET USER KNOW SOMETHING IS HAPPENING
		$buttonSubmit.Text = "Please wait..." #Updates button text for status
		$buttonSubmit.Refresh()
		
		$date = Get-Date -Format G #GET TODAYS DATE
		
		#Gets the selected user object
		$usrClass = Get-SCSMClass -Name Microsoft.AD.User$ -ComputerName $SCSMServer
		$usr = Get-SCSMObject -Class $usrClass -Filter "Username -eq $($listUsers.SelectedItem.ToString())" -ComputerName $SCSMServer
		
		#Gets the hardware asset object
		$hwaClass = Get-SCSMClass -Name Cireson.AssetManagement.HardwareAsset$ -ComputerName $SCSMServer
		
		#GET ENUMERATION VALUES
		$statusEnum = Get-SCSMEnumeration -Name Cireson.AssetManagement.HardwareAssetStatusEnum.Loaned$ -ComputerName $SCSMServer
		
		#GETS THE REQUIRED RELATIONSHIP CLASS
		$custRelClass = Get-SCSMRelationshipClass -Id 'cbb45424-b0a2-72f0-d535-541941cdf8e1' -ComputerName $SCSMServer
		
		foreach ($row in $searchGrid.SelectedRows)
		{
			#SET ASSET VARIABLE FOR CURRENT ASSET NAME
			$assetName = $row.Cells["Asset Name"].Value.ToString()
			$hwa = Get-SCSMObject -Class $hwaClass -Filter "DisplayName -eq $AssetName" -ComputerName $SCSMServer
			
			#Remove old custodian
			Get-SCSMRelationshipObject -BySource $hwa -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
			
			#Add new custodian
			New-SCSMRelationshipObject -Relationship $custRelClass -Source $hwa -Target $usr -Bulk -ComputerName $SCSMServer
			
			#Blank out required values
			$propertyHash = @{
				"AssignedDate" = $date;
				"LoanedDate" = $date;
				"LoanReturnedDate" = $datepickerAssigned.Value;
				"HardwareAssetStatus" = $statusEnum
			}
			$hwa | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
		}
		#Close the form on submit
		$subForm.Close()
		LoadGrid
	}
	
	$subForm = New-Object System.Windows.Forms.Form
	$subForm.Size = New-Object System.Drawing.Size(225, 230)
	$subForm.StartPosition = 'CenterScreen'
	$subForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedToolWindow
	$subForm.Text = "Associate Loaned Asset"
	
	#Form Elements
	$textUsername = New-Object System.Windows.Forms.TextBox
	$textUsername.Location = New-Object System.Drawing.Size(10, 10)
	$textUsername.Size = New-Object System.Drawing.Size(200, 20)
	$textUsername.Text = "Type Enterprise ID"
	$textUsername.add_KeyDown({ if ($_.KeyCode -eq 'Enter') { SearchForUser } })
	$subForm.Controls.Add($textUsername)
	
	$buttonSearch = New-Object System.Windows.Forms.Button
	$buttonSearch.Location = New-Object System.Drawing.Size(10, 35)
	$buttonSearch.Size = New-Object System.Drawing.Size(200, 20)
	$buttonSearch.Text = "Search"
	$buttonSearch.add_Click({ searchForuser })
	$subForm.Controls.Add($buttonSearch)
	
	$listUsers = New-Object System.Windows.Forms.ListBox
	$listUsers.Location = New-Object System.Drawing.Size(10, 60)
	$listUsers.Size = New-Object System.Drawing.Size(200, 60)
	$listUsers.SelectionMode = 'One'
	$subForm.Controls.Add($listUsers)
	
	$labelAssignedDate = New-Object System.Windows.Forms.Label
	$labelAssignedDate.Location = New-Object System.Drawing.Size(10, 120)
	$labelAssignedDate.Size = New-Object System.Drawing.Size(200, 20)
	$labelAssignedDate.Text = "Select the Loan Return Date'"
	$subForm.Controls.Add($labelAssignedDate)
	
	$datepickerAssigned = New-Object System.Windows.Forms.DateTimePicker
	$datepickerAssigned.Location = New-Object System.Drawing.Size(10, 140)
	$datepickerAssigned.Size = New-Object System.Drawing.Size(200, 20)
	$subForm.Controls.Add($datepickerAssigned)
	
	$buttonSubmit = New-Object System.Windows.Forms.Button
	$buttonSubmit.Location = New-Object System.Drawing.Size(10, 165)
	$buttonSubmit.Size = New-Object System.Drawing.Size(200, 30)
	$buttonSubmit.Text = "Submit"
	$buttonSubmit.add_Click({ SubmitChanges })
	$subForm.Controls.Add($buttonSubmit)
	
	#Display Form
	$subForm.Add_Shown({ $subForm.Activate() })
	[void] $subForm.ShowDialog()
}

function AssociateCustodian
{
	#Functions
	function SearchForUser
	{
		$buttonSearch.Text = "Please wait..." #Updates button text for status
		$buttonSearch.Refresh()
		
		#Reset text box back to defaults and remove existing items
		$listUsers.BackColor = 'White'
		$listUsers.Items.Clear()
		
		#Retreive the user class object and all users that match the search string
		$class = Get-SCSMClass -Name Microsoft.AD.User$ -ComputerName $SCSMServer
		$users = Get-SCSMObject -Class $class -Filter "Username -like $('*' + $textUsername.Text.ToString() + '*')" -ComputerName $SCSMServer
		
		#Fill the list box with the users that match the query
		foreach ($user in $users)
		{
			$listUsers.Items.Add($user.Username)
		}
		
		$buttonSearch.Text = "Search" #Updates button text for status
		$buttonSearch.Refresh()
	}
	
	function SubmitChanges
	{
		#STOP THE SUBMIT AND HIGHLIGHT THE REQUIRED VALUE
		if ($listUsers.SelectedItem -eq $null)
		{
			$listUsers.BackColor = 'LightPink'
			return 1
		}
		
		#UPDATE BUTTON LABEL TO LET USER KNOW SOMETHING IS HAPPENING
		$buttonSubmit.Text = "Please wait..." #Updates button text for status
		$buttonSubmit.Refresh()
		
		#SETUP VARIABLES
		$hwaClass = Get-SCSMClass -Name Cireson.AssetManagement.HardwareAsset$ -ComputerName $SCSMServer
		$usrClass = Get-SCSMClass -Name Microsoft.AD.User$ -ComputerName $SCSMServer
		$custRelClass = Get-SCSMRelationshipClass -Id 'cbb45424-b0a2-72f0-d535-541941cdf8e1' -ComputerName $SCSMServer
		
		foreach ($row in $searchGrid.SelectedRows)
		{
			#SET ASSET VARIABLE FOR CURRENT ASSET NAME
			$assetName = $row.Cells["Asset Name"].Value.ToString()
			$hwa = Get-SCSMObject -Class $hwaClass -Filter "DisplayName -eq $AssetName" -ComputerName $SCSMServer
			$usr = Get-SCSMObject -Class $usrClass -Filter "Username -eq $($listUsers.SelectedItem.ToString())" -ComputerName $SCSMServer
			
			#REMOVE ANY EXISTING CUSTODIAN, ADD NEW CUSTODIAN
			Get-SCSMRelationshipObject -BySource $hwa -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
			
			#ADD NEW CUSTODIAN RELATIONSHIP
			New-SCSMRelationshipObject -Relationship $custRelClass -Source $hwa -Target $usr -Bulk -ComputerName $SCSMServer
			
			#GET ENUMERATION VALUES
			$statusEnum = Get-SCSMEnumeration -Name Cireson.AssetManagement.HardwareAssetStatusEnum.Deployed$ -ComputerName $SCSMServer
			
			#SET OTHER REQUIRED VALUES
			$propertyHash = @{
				"AssignedDate" = (Get-Date -Format G);
				"HardwareAssetStatus" = $statusEnum
			}
			
			$hwa | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
		}
		
		#Close the form on submit
		$subForm.Close()
		LoadGrid
	}
	$subForm = New-Object System.Windows.Forms.Form
	$subForm.Size = New-Object System.Drawing.Size(225, 190)
	$subForm.StartPosition = 'CenterScreen'
	$subForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedToolWindow
	$subForm.Text = "Associate Custodian"
	
	#Form Elements
	$textUsername = New-Object System.Windows.Forms.TextBox
	$textUsername.Location = New-Object System.Drawing.Size(10, 10)
	$textUsername.Size = New-Object System.Drawing.Size(200, 20)
	$textUsername.Text = "Enter Enterprise ID"
	$textUsername.add_KeyDown({ if ($_.KeyCode -eq 'Enter') { SearchForUser } })
	$subForm.Controls.Add($textUsername)
	
	$buttonSearch = New-Object System.Windows.Forms.Button
	$buttonSearch.Location = New-Object System.Drawing.Size(10, 35)
	$buttonSearch.Size = New-Object System.Drawing.Size(200, 20)
	$buttonSearch.Text = "Search"
	$buttonSearch.add_Click({ SearchForUser })
	$subForm.Controls.Add($buttonSearch)
	
	$listUsers = New-Object System.Windows.Forms.ListBox
	$listUsers.Location = New-Object System.Drawing.Size(10, 60)
	$listUsers.Size = New-Object System.Drawing.Size(200, 60)
	$listUsers.SelectionMode = 'One'
	$subForm.Controls.Add($listUsers)
	
	$buttonSubmit = New-Object System.Windows.Forms.Button
	$buttonSubmit.Location = New-Object System.Drawing.Size(10, 120)
	$buttonSubmit.Size = New-Object System.Drawing.Size(200, 30)
	$buttonSubmit.Text = "Submit"
	$buttonSubmit.add_Click({ SubmitChanges })
	$subForm.Controls.Add($buttonSubmit)
	
	#Display Form
	$subForm.Add_Shown({ $subForm.Activate() })
	[void] $subForm.ShowDialog()
}

function ChangeStatus
{
	#Functions
	function SubmitChanges
	{
		if ($comboStatus.SelectedItem -eq $null) { $comboStatus.BackColor = 'LightPink'; return 1 }	#Verifies that a selection has been made
		$buttonSubmit.Text = "Please wait..." #Updates button text for status
		$buttonSubmit.Refresh()
		
		$date = Get-Date -Format G #GET TODAYS DATE
		
		#Retreive the user class object and all users that match the search string
		$enum = Get-SCSMEnumeration -Name Cireson.AssetManagement.HardwareAssetStatusEnum$ -ComputerName $SCSMServer
		
		#Get the hardware asset class
		$hwaClass = Get-SCSMClass -Name Cireson.AssetManagement.HardwareAsset$ -ComputerName $SCSMServer
		
		foreach ($row in $searchGrid.SelectedRows)
		{
			#SET ASSET VARIABLE FOR CURRENT ASSET NAME
			$assetName = $row.Cells["Asset Name"].Value.ToString()
			$hwaObj = Get-SCSMObject -Class $hwaClass -Filter "DisplayName -eq $assetName" -ComputerName $SCSMServer
			
			#Depending on selected value peforms requested actions:
			#loaned, deployed = These are hidden now, by request.
			#in stock = {custodian = blank, assigned date = today,  loaned date = blank, loan return date = blank}
			#ordered ={custodian = blank, ordered date = today}
			#aap engaged, legal letter 1, loss employee, loss exit, loss theft = {no workflow change required}
			#received ={custodian = blank, received date = today}
			#retired, disposed, rma, write off = {custodian = blank, disposed date = today,  loaned date = blank, loan return date = blank}
			#damaged, pending disposal = { custodian = blank}
			
			if ($comboStatus.SelectedItem.ToString() -eq "Ordered")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"OrderedDate" = $date;
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			elseif ($comboStatus.SelectedItem.ToString() -eq "In Stock")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"AssignedDate" = $date;
					"LoanedDate" = $null;
					"LoanReturnedDate" = $null;
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			elseif ($comboStatus.SelectedItem.ToString() -eq "Received")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"ReceivedDate" = $date;
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			elseif ($comboStatus.SelectedItem.ToString() -eq "Retired")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"DisposalDate" = $date;
					"LoanedDate" = $null;
					"LoanReturnedDate" = $null;
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			elseif ($comboStatus.SelectedItem.ToString() -eq "Disposed")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"DisposalDate" = $date;
					"LoanedDate" = $null;
					"LoanReturnedDate" = $null;
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			elseif ($comboStatus.SelectedItem.ToString() -eq "RMA")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"DisposalDate" = $date;
					"LoanedDate" = $null;
					"LoanReturnedDate" = $null;
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			elseif ($comboStatus.SelectedItem.ToString() -eq "Write Off")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"DisposalDate" = $date;
					"LoanedDate" = $null;
					"LoanReturnedDate" = $null;
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			elseif ($comboStatus.SelectedItem.ToString() -eq "Damaged")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			elseif ($comboStatus.SelectedItem.ToString() -eq "Pending Disposal")
			{
				#REMOVE ANY EXISTING CUSTODIAN
				Get-SCSMRelationshipObject -BySource $hwaObj -Filter "RelationshipId -eq 'cbb45424-b0a2-72f0-d535-541941cdf8e1'" -ComputerName $SCSMServer | Remove-SCSMRelationshipObject -ComputerName $SCSMServer
				#BUILD PROPERTIES
				$propertyHash = @{
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
			else
			{
				#BUILD PROPERTIES
				$propertyHash = @{
					"HardwareAssetStatus" = (Get-SCSMChildEnumeration -Enumeration $enum -ComputerName $SCSMServer | where { $_.DisplayName -eq $comboStatus.SelectedItem.ToString() })
				}
				#SET ORDER DATE
				$hwaObj | Set-SCSMObject -PropertyHashtable $propertyHash -ComputerName $SCSMServer
			}
		}
		#Close the form on submit
		$subForm.Close()
		LoadGrid
	}
	
	#MAIN FORM
	$subForm = New-Object System.Windows.Forms.Form
	$subForm.Size = New-Object System.Drawing.Size(225, 100)
	$subForm.StartPosition = 'CenterScreen'
	$subForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedToolWindow
	$subForm.Text = "Change Asset Status"
	
	#Form Elements
	$comboStatus = New-Object System.Windows.Forms.ComboBox
	$comboStatus.Location = New-Object System.Drawing.Size(10, 10)
	$comboStatus.Size = New-Object System.Drawing.Size(200, 20)
	$comboStatus.DropDownHeight = 70
	$subForm.Controls.Add($comboStatus)
	
	$buttonSubmit = New-Object System.Windows.Forms.Button
	$buttonSubmit.Location = New-Object System.Drawing.Size(10, 35)
	$buttonSubmit.Size = New-Object System.Drawing.Size(200, 30)
	$buttonSubmit.Text = "Submit"
	$buttonSubmit.add_Click({ SubmitChanges })
	$subForm.Controls.Add($buttonSubmit)
	
	#LOADS THE LOCATIONS LIST AFTER BUTTON PRESS
	if (Get-Job -Id $jobIdStatus -ErrorAction 'SilentlyContinue')
	{
		while ((Get-Job -Id $jobIdStatus).State -eq 'Running') { Start-Sleep -Milliseconds 5 }
		$Script:statusList = Receive-Job -Id $jobIdStatus | sort DisplayName
		foreach ($status in $statusList)
		{
            if(($status.displayname -ne 'Deployed') -and ($status.displayname -ne 'Loaned'))
            {
			    $comboStatus.Items.Add($status)
            }
		}
		Remove-Job -Id $jobIdStatus
	}
	else
	{
		foreach ($status in $statusList)
		{
			if(($status.displayname -ne 'Deployed') -and ($status.displayname -ne 'Loaned'))
            {
			    $comboStatus.Items.Add($status)
            }
		}
	}
	
	#Display Form
	$subForm.Add_Shown({ $subForm.Activate() })
	[void] $subForm.ShowDialog()
}

#PRE-FORM DISPLAY AREA ===============================================================================
#PERFORMS AN ASSET SEARCH ON FORM LOAD IF PASSED AS A PARAMETER
$mainForm.add_Activated({ 
    if($startingAsset -ne $null)
    {
        SearchByAssetName($startingAsset);
        $script:startingAsset = $null
    } 
    elseif ($searchGrid.RowCount.Equals(0))
    {
    	DisableButtons
    } 
})

#Display Form =========================================================================================
$mainForm.Add_Shown({ $mainForm.Activate() })
[void] $mainForm.ShowDialog()

#POST-FORM DISPLAY AREA ===============================================================================
